 #+TITLE:Here is my check list for 2-3 days short trip:
 
* Clothes
   - [ ] Socks * 2
   - [ ] Underwears * 2
   - [ ] Shirts * 3
   - [ ] Swimsuit [Optional]
* Medical
   - [ ] Toothbrush
   - [ ] Toothpaste
   - [ ] Hair Brush
   - [ ] Pain Killers
* Food or Drinks
   - [ ] Lunch or Dinner
   - [ ] Bottle of cold water
   - [ ] Gum
* Gadgets
** Laptop
   - [ ] Laptop
   - [ ] Mouse & MousePad
   - [ ] Charger
   - [ ] Ext. HDD
** Mobile
   - [ ] Mobile
   - [ ] HeadPhones
   - [ ] Charger
* Other
   - [ ] Money
   - [ ] Tickets
